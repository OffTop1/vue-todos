import Vue from "vue";
import VueRouter from "vue-router";
import TodosList from "../views/TodosList.vue";
import TodoShow from "../views/TodoShow.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "todos-list",
    component: TodosList
  },
  {
    path: "/create-todo",
    name: "create-todo",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "create" */ "../views/TodoCreate.vue")
  },
  {
    path:"/todo/:id",
    name:"todo-show",
    component: TodoShow,
    props:true
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
