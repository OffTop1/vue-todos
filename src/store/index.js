import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  state: {
    todos: []
  },
  mutations: {
    
    ADD_TODO(state, todo){
      state.todos.push(todo)
      localStorage.setItem('todos', state.todos);
    },
    REMOVE_TODO(state, id){
     state.todos = state.todos.filter(todo => todo.id !== id)
    },
    UPDATE_TODO_FROM_TODOLIST(state, {newTask}){
      let idx = state.todos.findIndex(todo => todo.id == newTask.idx);

      state.todos[idx].todolist[newTask.taskIdx] = {
        id:newTask.id, done:newTask.done, name:newTask.name};
    },
    REMOVE_TASK_FROM_TODOLIST(state, {todoId, taskIdx}){
      let idx = null;
      state.todos.forEach((todo, index) => {
        if(todoId == todo.id){
          idx = index;
        }
      });
      state.todos[idx].todolist = state.todos[idx].todolist.filter((task, index)=>{
        return index != taskIdx
      })


    },
    UPDATE_TODOLIST_TITLE(state, {todoId, newTitle}){
      let idx = null;
      state.todos.forEach((todo, index) => {
        if(todoId == todo.id){
          idx = index;
        }
      });
      state.todos[idx].title = newTitle;
    }
  },
  actions: {
    
    createTodo({commit}, todo){
      console.log(todo)
      commit('ADD_TODO', todo)
    },
    removeTodo({commit}, todoId){
      commit('REMOVE_TODO', todoId)
    },
    updateTodoFromTodolist({commit}, {newTask}){
      commit('UPDATE_TODO_FROM_TODOLIST', {newTask})
    },
    removeTaskFromTodolist({commit}, {todoId, taskIdx}){
      console.log(todoId, taskIdx)
      console.log("delete")
      commit('REMOVE_TASK_FROM_TODOLIST', {todoId, taskIdx})
    },
    updateTodolistTitle({commit}, {todoId, newTitle}){
      commit('UPDATE_TODOLIST_TITLE', {todoId, newTitle})
    }
  },
  getters:{
    getTodoById: state => id => {
      return state.todos.find(todo => todo.id == id)
    },

  },
  modules: {},
  plugins: [vuexLocal.plugin]
});
